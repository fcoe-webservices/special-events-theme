<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!--  HEADER  -->

  <header class="page-header -homePage" id="header">
    <div class="container row collapse -homePage">

      <div class="large-12 columns site-hero -homePage">
        <nav class="top-bar" data-topbar="" role="navigation" data-options="is_hover: false">
          <ul class="title-area">
            <!-- Title Area -->
            <li class="name">
              <h2><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h2>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span><strong>Nav</strong></span></a></li>
          </ul>
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="left">
              <?php print theme('links__system_main_menu', array('links' => $main_menu)); ?>
            </ul> 
          </section>
        </nav>
          <?php print render($page['header']); ?>
   
      </div>
      
    </div>
  </header> <!-- /header -->

  <!--  MAIN  -->

  <div id="main">
    <div class="container row">

      <?php if ($title|| $messages || $tabs || $action_links): ?>
        <?php if ($page['highlighted']): ?>
          <div id="highlighted"><?php print render($page['highlighted']) ?></div>
        <?php endif; ?>

        <?php print $messages; ?>
        <?php print render($page['help']); ?>

        <?php if ($tabs): ?>
          <div class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>

        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
      <?php endif; ?>

      <section id="content" class="large-12 columns">

        <div id="content-header" class="content-header row -homePage">
          <div class="large-12 columns">

          <?php if ($page['content_header']): ?>
            <?php print render($page['content_header']); ?>
          <?php endif; ?>
          </div>

        </div> <!-- /#content-header -->

        <!--  CONTENT AREA  -->

        <div id="content-area" class="content-area container -homePage">

          <!-- <?php print render($page['content']) ?> -->

          <?php if ($page['sidebar_second']): ?>
            <div id="sidebar-second" class="large-3 large-pull-9 columns sidebar second show-for-large-up">
              <?php print render($page['sidebar_second']); ?>
            </div>
            <!-- /sidebar-second -->
          <?php endif; ?>

        </div> <!-- /content-area -->

      </section> <!-- /content -->
    </div> <!-- /container row -->


  </div> <!-- /main -->

<?php if ($page['content_footer']): ?>
    <div id="content-footer" class="content-footer -homePage">
        <?php print render($page['content_footer']); ?>
    </div> <!-- /#content-footer -->
<?php endif; ?>



  <footer>
    <div class="row">
      <?php print render($page['footer']); ?>
    </div>
  </footer>

</div> <!-- /page -->