<div class="fcoeHeaderWrap">
  <div class="fcoeHeader row">
    <div class="fcoeNameplate columns large-6">
    	<img src="/sites/all/themes/specialevents/images/fcoe-logo-white-50px.png">
    	<div class="title">
      	<h3 class="fcoeNameplate__title"><a href="https://fcoe.org/">Fresno County Superintendent of Schools</a></h3>
      	<h4 class="fcoeNameplate__superintendent">Dr. Michele Cantwell-Copher, Superintendent</h4>
    	</div>
    </div>
    <nav class="fcoeNav show-for-large-up columns large-6">
      <ul class="fcoeNav__menu row">
        <li class="fcoeNav__menuItem"><a class="fcoeNav__menuLink first" href="https://fcoe.org/office-education">Office of Education</a></li>
        <li class="fcoeNav__menuItem"><a class="fcoeNav__menuLink" href="https://fcoe.org/departments">Departments</a></li>
        <li class="fcoeNav__menuItem"><a class="fcoeNav__menuLink" href="https://fcoe.org/districts">Districts</a></li>
        <li class="fcoeNav__menuItem"><a class="fcoeNav__menuLink" href="https://fcoe.org/events">Events</a></li>
        <li class="fcoeNav__menuItem"><a class="fcoeNav__menuLink last" href="https://fcoe.org/news">News</a></li>
      </ul>
    </nav>
  </div>
  <a class="right-off-canvas-toggle hide-for-large-up menu-icon" href="#" ><img src="/<?php print path_to_theme(); ?>/images/navicon.svg" /> <span>Menu</span></a>
  <aside class="right-off-canvas-menu hide-for-large-up">
      <ul>
        <li><a class="first" href="https://fcoe.org/office-education">Office of Education</a></li>
        <li><a href="https://fcoe.org/departments">Departments</a></li>
        <li><a href="https://fcoe.org/distric">Districts</a></li>
        <li><a href="https://fcoe.org/events">Events</a></li>
        <li><a class="last" href="https://fcoe.org/news">News</a></li>
      </ul>
  </aside>
  <a class="exit-off-canvas"></a>
</div>