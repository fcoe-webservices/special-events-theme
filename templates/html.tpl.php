<!DOCTYPE html>
<html>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

  <?php print $styles; ?>
  <?php print $head_scripts; ?>
  <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">  
      <div id="skip">
        <a href="#content"><?php print t('Jump to Navigation'); ?></a>
      </div>

      <?php require('includes/fcoe_header.inc');  ?>

      <?php print $page; ?>
      <?php print $scripts; ?>
      <?php print $page_bottom; ?>
    </div>

  </div>
</body>
</html>