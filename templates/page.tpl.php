<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!--  HEADER  -->

  <header id="header" class="page-header">
  <div class="page-header-inner">
    <div class="container row collapse" >

      <div class="large-12 columns site-hero">
        <nav class="top-bar" data-topbar="" role="navigation" data-options="is_hover: false">
          <ul class="title-area">
            <!-- Title Area -->
            <li class="name">
              <h2><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h2>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span><strong>Nav</strong></span></a></li>
          </ul>
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="left">
              <?php print theme('links__system_main_menu', array('links' => $main_menu)); ?>
            </ul> 
          </section>
        </nav>
      <!-- <div class="register-nag">January 30 &amp; February 6, 2016<a href="/register">Register Now</a></div> -->
      
      </div>
      

    </div>
  </div>
  </header> <!-- /header -->

  <!--  MAIN  -->

  <div id="main" class="row">
    <div class="container">

      <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']) ?></div>
      <?php endif; ?>

      <?php print $messages; ?>
      <?php print render($page['help']); ?>

      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <section id="content" class="large-12 columns">


			<?php if (!$is_front && !empty($title)): ?>
        <div class="content-header" >  
          <h1 class="contentHeaderTitle"><?php print $title; ?></h1>
        </div> <!-- /#content-header -->
			<?php endif; ?>
        

        <?php if ($tabs): ?>
          <div class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>

          <div id="content-area" class="content-area container">

            <?php print render($page['content']) ?>

            <?php if ($page['sidebar_second']): ?>
              <div id="sidebar-second" class="large-3 large-pull-9 columns sidebar second show-for-large-up">
                <?php print render($page['sidebar_second']); ?>
              </div>
              <!-- /sidebar-second -->
            <?php endif; ?>

          </div>

      </section> <!-- /content-inner /content -->

    </div>
  </div> <!-- /main -->

  <footer>
    <div class="row">
      <div class="large-12 columns">
        <?php print render($page['footer']); ?>
      </div>
    </div>
  </footer>

</div> <!-- /page -->
