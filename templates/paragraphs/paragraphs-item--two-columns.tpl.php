<h3><?php print render($content['field_section_title']); ?></h3>
<div class="row two-column-paragraph">
	<div class="columns large-6 column-1">
		<?php print render($content['field_p_col1_text']); ?>
	</div>
	<div class="columns large-6 column-2">
		<?php print render($content['field_p_col2_text']); ?>
		<?php print render($content['field_block_reference']); ?>
	</div>
</div>
