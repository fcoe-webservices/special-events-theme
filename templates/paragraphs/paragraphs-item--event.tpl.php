<div class="row event-detail">
		<div class="columns large-12">
			<h3><?php print render($content['field_p_event_title']); ?></h3>
			<?php print render($content['field_p_event_info']); ?>
		</div>
	<div class="event-details columns large-6">
		<h3 class="doc-list-heading">Event Details</h3>
		<div class="event-date">
			<?php print render($content['field_p_event_date']); ?>
		</div>
		<div class="event-location">
			<?php print render($content['field_p_event_address']); ?>
		</div>
	</div>
	<div class="event-map columns large-6">
		<div class="google-maps">
			<?php print render($content['field_p_event_map']); ?>
		</div>
	</div>
</div>