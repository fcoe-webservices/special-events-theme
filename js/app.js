(function($) {
  $(document).foundation({

      offcanvas : {
        // Sets method in which offcanvas opens.
        // [ move | overlap_single | overlap ]
        open_method: 'overlap'
      }

    });
}(jQuery));