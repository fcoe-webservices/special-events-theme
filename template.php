<?php

/* Implements https://gist.github.com/pascalduez/1418121 */

/**
 * Implements hook_preprocess_html().
 */
function specialevents_preprocess_html(&$vars) {
  // Move JS files "$scripts" to page bottom for perfs/logic.
  // Add JS files that *needs* to be loaded in the head in a new "$head_scripts" scope.
  // Reference the Gruntfile for a list of js scripts to be bundled.
  $path = drupal_get_path('theme', 'specialevents');
  drupal_add_js($path . '/js/production.top.js', array('scope' => 'head_scripts', 'weight' => -1, 'preprocess' => FALSE));  
}

/**
 * Implements hook_process_html().
 */
function specialevents_process_html(&$vars) {
  $vars['head_scripts'] = drupal_get_js('head_scripts');
}

/**
 * Override or insert variables into the block template
 */
function specialevents_preprocess_block(&$vars) {

  
  // controls the page-title class on h2
  $block = $vars['block'];

  if ( $block->region == 'content_header' && drupal_is_front_page() ) {
    $vars['title_attributes_array']['class'][] = 'contentHeaderTitle -homePage';
  } else if ( $block->region == 'content_footer' && drupal_is_front_page() ) {
    $vars['title_attributes_array']['class'][] = 'contentFooterTitle title -homePage';
  }

}